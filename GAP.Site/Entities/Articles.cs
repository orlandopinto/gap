﻿using System.ComponentModel.DataAnnotations;

namespace GAP.Site.Entities
{
	public class Articles
	{
		[Required, Display(Name = @"ID"), DataType(DataType.Text)]
		public int id { get; set; }

		[Required, Display(Name = @"Name:"), StringLength(50), DataType(DataType.Text)]
		public string name { get; set; }
		
		[Required, Display(Name = @"Description:"), StringLength(500), DataType(DataType.Text)]
		public string description { get; set; }
		
		[Required, Display(Name = @"Price:"), DataType(DataType.Currency)]
		public decimal price { get; set; }
		
		[Required, Display(Name = @"Total in Shelf:"), DataType(DataType.Text)]
		public int total_in_shelf { get; set; }
		
		[Required, Display(Name = @"Total in Vault:"), DataType(DataType.Text)]
		public int total_in_vault { get; set; }
		
		[Required, Display(Name = @"Store:"), DataType(DataType.Text)]
		public int store_id { get; set; }
	}
}
