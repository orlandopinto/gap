﻿using System.Collections.Generic;

namespace GAP.Site.Entities
{
	public class StoresList
	{
		public List<Stores> Stores { get; set; }
		public bool success { get; set; }
		public int total_elements { get; set; }
	}
}