﻿using System.ComponentModel.DataAnnotations;

namespace GAP.Site.Entities
{
	public class Stores
	{
		[Display(Name = "ID")]
		public int id { get; set; }

		[Required, Display(Name = "Name:"), StringLength(250), DataType(DataType.Text)]
		public string name { get; set; }

		[Required, Display(Name = "Address:"), StringLength(250), DataType(DataType.Text)]
		public string address { get; set; }
	}
}