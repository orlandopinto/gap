﻿using GAP.Site.Entities;

namespace GAP.Site.Interfaces
{
	public interface IArticles
	{
		string Add(Articles Article);
		string Delete(int id);
		ArticlesList GetAll();
		ArticlesList GetArticlesByStore(int id);
		Articles GetById(int id);
		string Update(int id, Articles Article);
	}
}
