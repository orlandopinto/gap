﻿namespace GAP.Service.Models
{
	public class Articles
	{
		public int id { get; set; }
		public string name { get; set; }
		public string description { get; set; }
		public decimal price { get; set; }
		public int total_in_shelf { get; set; }
		public int total_in_vault { get; set; }
		public int store_id { get; set; }
	}
}