﻿using System.Collections.Generic;

namespace GAP.Service.Models
{
	public class StoresList
	{
		public List<Stores> Stores { get; set; }
		public bool success { get; set; }
		public int total_elements { get; set; }
	}
}