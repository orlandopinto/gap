﻿using GAP.Service.EF;
using GAP.Service.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using static GAP.Service.Utilities;

namespace GAP.Service.Controllers
{
	[Authorize]
	public class StoresController : ApiController
	{
		private GAPEntities db = new GAPEntities();

		[Authorize]
		public IHttpActionResult GetStores()
		{
			string result = string.Empty;
			HttpStatusCode resultStatus = HttpStatusCode.OK;
			try
			{
				var list = db.Stores;
				StoresList Lista = new StoresList();
				Lista.Stores = new List<Models.Stores>();
				foreach (var item in list)
					Lista.Stores.Add(new Models.Stores() { id = item.id, name = item.name, address = item.address });

				Lista.success = true;
				Lista.total_elements = list.Count();
				result = JsonConvert.SerializeObject(Lista);
			}
			catch (Exception ex)
			{
				result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 500, error_msg = ex.Message, success = false });
				resultStatus = HttpStatusCode.InternalServerError;
			}
			return Content(resultStatus, result);
		}

		[Authorize]
		[ResponseType(typeof(Models.Stores))]
		public IHttpActionResult GetStore(int id)
		{
			string result = string.Empty;
			HttpStatusCode resultStatus = HttpStatusCode.OK;
			try
			{
				var data = db.Stores.Find(id);
				if (data == null)
				{
					result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 404, error_msg = @"Record not found", success = false });
					resultStatus = HttpStatusCode.NotFound;
				}
				else
					result = JsonConvert.SerializeObject(new Models.Stores() { id = data.id, name = data.name, address = data.address });
			}
			catch (Exception ex)
			{
				result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 500, error_msg = ex.Message, success = false });
				resultStatus = HttpStatusCode.InternalServerError;
			}
			return Content(resultStatus, result);
		}

		[Authorize]
		[ResponseType(typeof(void))]
		public IHttpActionResult PutStore(int id, Models.Stores store)
		{
			string result = string.Empty;
			HttpStatusCode resultStatus = HttpStatusCode.OK;
			if (!ModelState.IsValid)
			{
				result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 400, error_msg = GetModelStateError(ModelState), success = false });
				resultStatus = HttpStatusCode.BadRequest;
			}
			if (id != store.id)
			{
				result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 404, error_msg = @"Record not found", success = false });
				resultStatus = HttpStatusCode.NotFound;
			}
			db.Entry(store).State = EntityState.Modified;
			try
			{
				db.SaveChanges();
				result = JsonConvert.SerializeObject(new Models.Stores() { id = store.id, name = store.name, address = store.address });
			}
			catch (Exception ex)
			{
				result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 500, error_msg = ex.Message, success = false });
				resultStatus = HttpStatusCode.InternalServerError;
			}
			return Content(resultStatus, result);
		}

		[Authorize]
		[ResponseType(typeof(Models.Stores))]
		public IHttpActionResult PostStore(Models.Stores store)
		{
			string result = string.Empty;
			HttpStatusCode resultStatus = HttpStatusCode.OK;
			if (!ModelState.IsValid)
			{
				result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 400, error_msg = GetModelStateError(ModelState), success = false });
				resultStatus = HttpStatusCode.BadRequest;
			}
			try
			{
                EF.Stores newStore = new EF.Stores() { id = store.id, name = store.name, address = store.address };
				db.Stores.Add(newStore);
				db.SaveChanges();
				result = JsonConvert.SerializeObject(new Models.Stores() { id = store.id, name = store.name, address = store.address });
			}
			catch (Exception ex)
			{
				if (StoreExists(store.id))
				{
					result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 500, error_msg = @"id duplicated", success = false });
					resultStatus = HttpStatusCode.InternalServerError;
				}
				else
					result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 500, error_msg = ex.Message, success = false });
			}
			return Content(resultStatus, result);
		}

		[Authorize]
		[ResponseType(typeof(Models.Stores))]
		public IHttpActionResult DeleteStore(int id)
		{
			string result = string.Empty;
			HttpStatusCode resultStatus = HttpStatusCode.OK;
			var data = db.Stores.Find(id);
			if (data == null)
			{
				result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 404, error_msg = @"Record not found", success = false });
				resultStatus = HttpStatusCode.NotFound;
			}
			else
			{
				try
				{
					db.Stores.Remove(data);
					db.SaveChanges();
					result = JsonConvert.SerializeObject(new Models.Stores() { id = data.id, name = data.name, address = data.address });
				}
				catch (Exception ex)
				{
					result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 500, error_msg = ex.Message, success = false });
					resultStatus = HttpStatusCode.InternalServerError;
				}
			}
			return Content(resultStatus, result);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing) db.Dispose();
			base.Dispose(disposing);
		}

		private bool StoreExists(int id)
		{
			return db.Stores.Count(e => e.id == id) > 0;
		}
	}
}